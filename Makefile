include theos/makefiles/common.mk

TWEAK_NAME = NSStringExtension
NSStringExtension_FILES = NSStringExtension/EDMutableObjectPair.m NSStringExtension/EDObjectPair.m NSStringExtension/NSString+Extensions.m

include $(THEOS_MAKE_PATH)/tweak.mk
